# Jim Mobile Red Phalox Edition

This repo contains a copy of the Jim Mobile Red source you can find on google code http://code.google.com/p/jim-mobile-red/. As development there has come to a halt, and I am use the app myself, I wanted to be able to update the source. Therefore I forked this project into this git repo.
If you discover new text messages that are not parsed by the program, please send an email to info-a|t-phalox.be

###Looking for the actual app?
This version is not available from the Android market (aka Play Store). There is however a compiles JimMobileRed.apk file available from this repository that you can install on your system. You will need to enable the option to 'install software from unreliably sources' in your system settings.

###This code was licensed under GPL V3, therefore I am obliged to do the same

This means that you are free:

+ **to Modify** — To “modify” a work means to copy from or adapt all or part of the work in a fashion requiring copyright permission, other than the making of an exact copy. The resulting work is called a “modified version” of the earlier work or a work “based on” the earlier work.
+ **to Propagate** — To “propagate” a work means to do anything with it that, without permission, would make you directly or secondarily liable for infringement under applicable copyright law, except executing it on a computer or modifying a private copy. Propagation includes copying, distribution (with or without modification), making available to the public, and in some countries other activities as well.
+ to make commercial use of the work

Under the following conditions:
**Attribution** — You must attribute the work in the manner specified by the author or licensor

**Attribution** in this case means that you should please provide a link to [Phalox.be](http://phalox.be/ "Phalox.be")

More information on this license can be found [here](http://www.gnu.org/licenses/gpl-3.0.html "GPL V3.0")

![CC BY](http://www.gnu.org/graphics/gplv3-88x31.png)
