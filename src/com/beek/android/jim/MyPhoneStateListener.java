package com.beek.android.jim;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class MyPhoneStateListener extends PhoneStateListener {
  Context mycontext;
  
  public MyPhoneStateListener(Context context) {
    mycontext = context;
  }
  public void onCallStateChanged(int state,String incomingNumber){  
  switch(state){  
    case TelephonyManager.CALL_STATE_IDLE:  
      Log.d("DEBUG", "IDLE");  
    break;  
    case TelephonyManager.CALL_STATE_OFFHOOK:  
      Log.d("DEBUG", "OFFHOOK");  
      SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mycontext);
      if (prefs.getBoolean("autoRefreshAfterPhoneCall", false)) {
        Util.sendSMS(mycontext, "", "CONSULT", "Quiet");
      }
      break;
    case TelephonyManager.CALL_STATE_RINGING:  
      Log.d("DEBUG", "RINGING");  
    break;  
    }  
  }   
}  