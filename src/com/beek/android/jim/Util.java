package com.beek.android.jim;

import java.lang.reflect.Method;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;
import android.widget.Toast;

public class Util {

	final static public String StringBetween(String text, String before,
			String after) {
		if (text.indexOf(before) == -1) {
			return ("");
		}
		;
		if (text.indexOf(after) == -1) {
			return ("");
		}
		;
		return text.substring(text.indexOf(before) + before.length(),
				text.indexOf(after));
	}

	final static public void SetAutomatedConsult(Context context,
			SharedPreferences prefs) {
		// send a consult regularly
		String refreshInterval = prefs.getString("automateConsult", "0");
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		Intent sendSMSIntent = new Intent(context, MainService.class);
		sendSMSIntent.setAction("com.beek.android.jim.sendsms");
		sendSMSIntent.putExtra("Type", "");
		sendSMSIntent.putExtra("Message", "CONSULT");
		sendSMSIntent.putExtra("Mode", "Quiet");
		// TODO : understand the PendingIntent flags properly and also the RTC
		// RTC_WAKEUP flags
		PendingIntent refreshBalancesPI = PendingIntent.getService(context, 0,
				sendSMSIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		if (!isANumber(refreshInterval) || refreshInterval.equals("0")) {
			alarmManager.cancel(refreshBalancesPI);
		} else {
			// TODO : understand the PendingIntent flags properly and also the
			// RTC RTC_WAKEUP flags
			alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
					System.currentTimeMillis() + (5 * 1000),
					Integer.parseInt(refreshInterval) * 60 * 1000,
					refreshBalancesPI);
		}
	}
	
	final static public boolean isANumber(String number)
	{
		boolean isIt = true;
		try {
			Integer.parseInt(number);
		}catch(Exception e){
			isIt = false;
		}
		return isIt;
	}
	/** send an SMS message to another device **/
	final static void sendSMS(Context context, String type, String message,
			String mode) {
		Intent sendSMSIntent = new Intent(context, MainService.class);
		sendSMSIntent.setAction("com.beek.android.jim.sendsms");
		sendSMSIntent.putExtra("Type", type);
		sendSMSIntent.putExtra("Message", message);
		sendSMSIntent.putExtra("Mode", mode);
		context.startService(sendSMSIntent);
	}

	final static public void SetScreenRefreshAtMidnight(Context context) {
		AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);
		PendingIntent updateScreenPI = PendingIntent.getBroadcast(context, 0,
				new Intent("update_jimRED_balances_screen"),
				PendingIntent.FLAG_CANCEL_CURRENT);
		Calendar c = Calendar.getInstance();
		c.set(Calendar.HOUR_OF_DAY, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 999);
		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP,
				c.getTimeInMillis() + 10, AlarmManager.INTERVAL_DAY,
				updateScreenPI);
	}

	@SuppressWarnings("rawtypes")
	final static public void SetDataConnection(Context context, boolean enableIt) {
		// Todo : once an official way exists to stop data connection use it !!
		// in the mean-time this works but it uses a private method from the
		// android core
		boolean isEnabled;
		Method dataConnSwitchmethod = null;
		Class telephonyManagerClass;
		Object ITelephonyStub;
		Class ITelephonyClass;

		final TelephonyManager telephonyManager = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);

		if (telephonyManager.getDataState() == TelephonyManager.DATA_CONNECTED) {
			isEnabled = true;
		} else {
			isEnabled = false;
		}
		try {
			telephonyManagerClass = Class.forName(telephonyManager.getClass()
					.getName());
			Method getITelephonyMethod = telephonyManagerClass
					.getDeclaredMethod("getITelephony");
			getITelephonyMethod.setAccessible(true);
			ITelephonyStub = getITelephonyMethod.invoke(telephonyManager);
			ITelephonyClass = Class
					.forName(ITelephonyStub.getClass().getName());

			if (isEnabled & !enableIt) {
				dataConnSwitchmethod = ITelephonyClass
						.getDeclaredMethod("disableDataConnectivity");
			} else if (!isEnabled && enableIt) {
				dataConnSwitchmethod = ITelephonyClass
						.getDeclaredMethod("enableDataConnectivity");
			}
			dataConnSwitchmethod.setAccessible(true);
			dataConnSwitchmethod.invoke(ITelephonyStub);
		} catch (Exception e) {
			Toast.makeText(context, "Exception " + e.toString(),
					Toast.LENGTH_LONG).show();
			if (enableIt) {
				Toast.makeText(context, "Can't Enable Data Connection",
						Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(context, "Can't Stop Data Connection",
						Toast.LENGTH_LONG).show();
			}
		}
	}
}
