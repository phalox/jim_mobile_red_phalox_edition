package com.beek.android.jim;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.widget.RemoteViews;

public class WidgetProvider1x1 extends AppWidgetProvider {

  @Override
  public void onReceive(Context context, Intent intent) {
    super.onReceive(context, intent);

    if (intent.getAction().equals("update_jimRED_balances_screen")) {

      AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
      ComponentName thisAppWidget = new ComponentName(context.getPackageName(), WidgetProvider1x1.class.getName());
      int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidget);

      onUpdate(context, appWidgetManager, appWidgetIds);
    }
  }

  @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
    // Perform this loop procedure for each App Widget that belongs to this
    // provider
    for (int i = 0; i < appWidgetIds.length; i++) {
      int appWidgetId = appWidgetIds[i];

      // Get the layout for the App Widget
      RemoteViews view = new RemoteViews(context.getPackageName(), R.layout.widget1x1_dark);

      /*
       * // Create an Intent to refresh the balances Intent intentRefresh = new Intent(context, MainService.class);
       * intentRefresh.setAction("com.beek.android.jim.sendsms"); intentRefresh.putExtra("Type", "");
       * intentRefresh.putExtra("Message", "CONSULT"); PendingIntent pendingIntentRefresh =
       * PendingIntent.getService(context, 0, intentRefresh, 0); // attach an on-click listener to the button to launch
       * a balance // refresh view.setOnClickPendingIntent(R.id.widget_layout1x1, pendingIntentRefresh);
       */

      // Create an Intent to launch the JimRed app
      Intent intentSMS = new Intent(context, MainActivity.class);
      PendingIntent pendingIntentSMS = PendingIntent.getActivity(context, 0, intentSMS, 0);
      // attach an on-click listener to the textfield
      view.setOnClickPendingIntent(R.id.widget_layout1x1, pendingIntentSMS);

      setTexts(context, view);

      // Tell the AppWidgetManager to perform an update on the current app
      // widget
      appWidgetManager.updateAppWidget(appWidgetId, view);
    }

    super.onUpdate(context, appWidgetManager, appWidgetIds);
  }

  private void setTexts(Context context, RemoteViews view) {
    SetTextAndColor(context, view, R.id.credit_text, "beltegoed");
    SetTextAndColor(context, view, R.id.sms_text, "allsms");
    SetTextAndColor(context, view, R.id.sms_base_text, "basesms");
    SetTextAndColor(context, view, R.id.call_text, "all");
    SetTextAndColor(context, view, R.id.call_base_text, "basecall");
    SetTextAndColor(context, view, R.id.data_text, "data");
  }

  private void SetTextAndColor(Context context, RemoteViews view, int id, String balancetype) {
    String text;
    int days, color = 1;
    SharedPreferences balances = context.getSharedPreferences("balances", 0);

    if (balancetype.equals("data")) { // for data change text a little (kB or MB)
      int kbytes;

      kbytes = Integer.parseInt(balances.getString("data_value", "0"));
      if (kbytes < 1000) {
        text = Integer.toString(kbytes).concat("k");
      } else {
        text = Integer.toString(kbytes / 1000).concat("M");
        // it should be 1024 technically speaking ... but I don't want more than 3 digits for k values
      }
    } else {
      text = balances.getString(balancetype.concat("_value"), "0");
    }

    long millesecs = 365;
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    String dateTo = balances.getString(balancetype.concat("_dateTo"), "");
    Calendar Today = Calendar.getInstance();
    Today.set(Calendar.HOUR_OF_DAY, 0);
    Today.set(Calendar.MINUTE, 0);
    Today.set(Calendar.SECOND, 0);
    Today.set(Calendar.MILLISECOND, 0);
    try {
      Date dateToDate = df.parse(dateTo);
      millesecs = (dateToDate.getTime() - Today.getTimeInMillis());
      days = (int) (millesecs / (1000L * 60L * 60L * 24L));
    } catch (ParseException e) {
      days = 365; // if no expiration - consider days is big
    }

    days += 1; // it's until expiration date included ! --> dates the same -> still 1 day to go
    if (days > 7) {
      color = Color.WHITE;
    } else if (days > 5) {
      color = Color.YELLOW;
    } else if (days > 3) {
      color = Color.rgb(255, 210, 0); // light orange
    } else if (days > 1) {
      color = Color.rgb(255, 160, 0); // orange
    } else if (days == 1) {
      color = Color.RED;
    } else { // it's expired !!
      color = Color.WHITE;
      text = "0";
    }

    view.setTextViewText(id, text);
    view.setTextColor(id, color);
  }
}
