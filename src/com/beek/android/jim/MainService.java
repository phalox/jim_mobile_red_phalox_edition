package com.beek.android.jim;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class MainService extends Service {
  public static final String TAG       = "JimRedSmsSenderService";
  public static final String SENT      = "SMS_SENT";
  public static final String DELIVERED = "SMS_DELIVERED";

  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    Log.i(TAG, TAG + " started with start id : " + startId + ": " + intent);

    if (intent.getAction().equals("com.beek.android.jim.sendsms")) {
      sendSMS(intent.getStringExtra("Type"), intent.getStringExtra("Message"), intent.getStringExtra("Mode"));
    }

    return START_NOT_STICKY;
  }

  private void sendSMS(String type, String message, String Mode) {
    boolean quiet = "Quiet".equals(Mode);
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
    String phoneNumber = prefs.getString("phoneNum", "1984");
    String pinCode = prefs.getString("pinCode", "12345");

    if (type.equals("T")) {
      message += " " + pinCode;
    }

    PendingIntent sentPI = null, deliveredPI = null;
    if ((!quiet) && (!prefs.getBoolean("noToastsForSmsStatus", false))) {
      sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
      deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

      // ---when the SMS has been sent---
      registerReceiver(new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
          switch (getResultCode()) {
          case Activity.RESULT_OK:
            Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
            break;
          case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
            Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
            break;
          case SmsManager.RESULT_ERROR_NO_SERVICE:
            Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
            break;
          case SmsManager.RESULT_ERROR_NULL_PDU:
            Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
            break;
          case SmsManager.RESULT_ERROR_RADIO_OFF:
            Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
            break;
          }
        }
      }, new IntentFilter(SENT));

      // ---when the SMS has been delivered---
      registerReceiver(new BroadcastReceiver() {
        @Override
        public void onReceive(Context arg0, Intent arg1) {
          switch (getResultCode()) {
          case Activity.RESULT_OK:
            Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
            break;
          case Activity.RESULT_CANCELED:
            Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
            break;
          }
        }
      }, new IntentFilter(DELIVERED));
    }

    SmsManager sms = SmsManager.getDefault();
    sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);

    if ((!quiet) && (!prefs.getBoolean("noToastsToJim", false))) {
      Toast.makeText(getBaseContext(), "Sending '" + message + "' to " + phoneNumber, Toast.LENGTH_SHORT).show();
    }
  }

}
